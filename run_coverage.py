#!/usr/bin/env python3

import argparse
import glob
import gzip
import itertools
import json
import os.path
import sys
import traceback

import yaml

try:
    from yaml import CSafeLoader as SafeLoader
except ImportError:
    from yaml import SafeLoader

D3M_PIPELINES = {
    'f596cd77-25f8-4d4c-a350-bb30ab1e58f6',
}


class PipelineNotFoundError(Exception):
    pass


def primitives_used_in_pipeline(pipeline):
    primitives = set()

    for step in pipeline.get('steps', []):
        if step['type'] == 'PRIMITIVE':
            primitives.add(step['primitive']['id'])
        elif step['type'] == 'SUBPIPELINE':
            primitives.update(primitives_used_in_pipeline(step['pipeline']))
        else:
            raise ValueError(f"Unsupported pipeline step type: {step['type']}")

    return primitives


def pipelines_used_in_pipeline_run(pipeline_run):
    pipeline_ids = set()

    pipeline_ids.add(pipeline_run['pipeline']['id'])
    if 'data_preparation' in pipeline_run['run']:
        pipeline_ids.add(pipeline_run['run']['data_preparation']['pipeline']['id'])
    if 'scoring' in pipeline_run['run']:
        pipeline_ids.add(pipeline_run['run']['scoring']['pipeline']['id'])

    return pipeline_ids


def resolve_pipeline(pipelines_dir, pipeline_id):
    pipeline_path = os.path.join(pipelines_dir, '{pipeline_id}.json'.format(pipeline_id=pipeline_id))
    try:
        with open(pipeline_path, 'r', encoding='utf8') as pipeline_file:
            return json.load(pipeline_file)
    except FileNotFoundError:
        pass

    for extension in ['yml', 'yaml']:
        pipeline_path = os.path.join(pipelines_dir, '{pipeline_id}.{extension}'.format(pipeline_id=pipeline_id, extension=extension))
        try:
            with open(pipeline_path, 'r', encoding='utf8') as pipeline_file:
                return yaml.load(pipeline_file, Loader=SafeLoader)
        except FileNotFoundError:
            pass

    if pipeline_id in D3M_PIPELINES:
        return None

    raise PipelineNotFoundError


def process_interface_version(interface_version):
    known_primitives = {}
    primitives_used_in_pipelines = set()
    has_errored = False

    for primitive_annotation_path in glob.iglob('{interface_version}/*/*/*/primitive.json'.format(interface_version=interface_version)):
        try:
            with open(primitive_annotation_path, 'r', encoding='utf8') as primitive_annotation_file:
                primitive_annotation = json.load(primitive_annotation_file)

            if primitive_annotation['id'] in known_primitives:
                has_errored = True
                print("Error: Duplicate primitive IDs, '{first_python_path}' and '{second_python_path}'.".format(
                    first_python_path=known_primitives[primitive_annotation['id']]['python_path'],
                    second_python_path=primitive_annotation['python_path'],
                ), flush=True)
            else:
                known_primitives[primitive_annotation['id']] = primitive_annotation
        except Exception:
            print("Error at primitive '{primitive_annotation_path}'.".format(primitive_annotation_path=primitive_annotation_path), flush=True)
            traceback.print_exc()
            sys.stdout.flush()
            has_errored = True

    for pipeline_run_path in itertools.chain(
        glob.iglob('{interface_version}/*/*/*/pipeline_runs/*.yaml.gz'.format(interface_version=interface_version)),
        glob.iglob('{interface_version}/*/*/*/pipeline_runs/*.yml.gz'.format(interface_version=interface_version)),
    ):
        pipelines_dir = os.path.sep.join(pipeline_run_path.split(os.path.sep)[:-2] + ['pipelines'])

        try:
            with gzip.open(pipeline_run_path, 'rt', encoding='utf8') as pipeline_run_file:
               pipeline_runs = yaml.load_all(pipeline_run_file, Loader=SafeLoader)

               for pipeline_run in pipeline_runs:
                    for pipeline_id in pipelines_used_in_pipeline_run(pipeline_run):
                        try:
                            pipeline = resolve_pipeline(pipelines_dir, pipeline_id)
                        except PipelineNotFoundError:
                            has_errored = True
                            print("Error: Could not resolve pipeline '{pipeline_id}' in '{pipelines_dir}' for pipeline run '{pipeline_run_path}'.".format(
                                pipeline_id=pipeline_id,
                                pipelines_dir=pipelines_dir,
                                pipeline_run_path=pipeline_run_path,
                            ), flush=True)
                            continue

                        if pipeline:
                            primitives_used_in_pipelines.update(primitives_used_in_pipeline(pipeline))

        except Exception:
            print("Error at pipeline run '{pipeline_run_path}'.".format(pipeline_run_path=pipeline_run_path), flush=True)
            traceback.print_exc()
            sys.stdout.flush()
            has_errored = True

    for primitive_id, primitive in known_primitives.items():
        if primitive_id not in primitives_used_in_pipelines:
            has_errored = True
            print(primitive['source']['name'], primitive['id'], primitive['python_path'], flush=True)

    return has_errored


def main():
    parser = argparse.ArgumentParser(description="Compute pipelines coverage.")
    parser.add_argument('interface_versions', metavar='INTERFACE', nargs='*', help="interface version(s) to compute coverage for", default=())
    arguments = parser.parse_args()

    has_errored = False
    for interface_version in arguments.interface_versions:
        has_errored = process_interface_version(interface_version) or has_errored

    if has_errored:
        sys.exit(1)


if __name__ == '__main__':
    main()
