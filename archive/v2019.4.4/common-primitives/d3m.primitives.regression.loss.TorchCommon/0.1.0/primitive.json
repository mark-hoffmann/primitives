{
    "algorithm_types": [
        "NEURAL_NETWORK_BACKPROPAGATION"
    ],
    "name": "Loss primitive (i.e., a PyTorch Criterion) for end-to-end training of primitive chains inheriting from GradientCompositionalityMixin",
    "primitive_family": "REGRESSION",
    "python_path": "d3m.primitives.regression.loss.TorchCommon",
    "source": {
        "name": "common-primitives",
        "contact": "mailto:gunes@robots.ox.ac.uk",
        "uris": [
            "https://gitlab.com/datadrivendiscovery/common-primitives/blob/master/common_primitives/loss.py",
            "https://gitlab.com/datadrivendiscovery/common-primitives.git"
        ]
    },
    "version": "0.1.0",
    "id": "f20ac610-1bf0-4401-b188-ef9dd8616a76",
    "installation": [
        {
            "type": "PIP",
            "package_uri": "git+https://gitlab.com/datadrivendiscovery/common-primitives.git@3df2ad23e1801e43166c64dc5767d847f232181d#egg=common-primitives"
        }
    ],
    "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/primitive.json",
    "original_python_path": "common_primitives.loss.Loss",
    "primitive_code": {
        "class_type_arguments": {
            "Inputs": "d3m.container.numpy.ndarray",
            "Outputs": "d3m.container.numpy.ndarray",
            "Params": "NoneType",
            "Hyperparams": "common_primitives.loss.Hyperparams"
        },
        "interfaces_version": "2019.4.4",
        "interfaces": [
            "base.GradientCompositionalityMixin",
            "transformer.TransformerPrimitiveBase",
            "base.PrimitiveBase"
        ],
        "hyperparams": {
            "loss_type": {
                "type": "d3m.metadata.hyperparams.Enumeration",
                "default": "mse",
                "structural_type": "str",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Type of end-to-end (pipeline) loss to be computed.",
                "values": [
                    "l1",
                    "mse",
                    "crossentropy"
                ]
            },
            "target_inputs": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": [
                    0
                ],
                "structural_type": "d3m.container.numpy.ndarray",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "End-to-end (pipeline) training targets. When produce is called, the inputs to this primitive will be compared with the targets (target_inputs), and a loss measuring how dissimilar they are will be returned."
            }
        },
        "arguments": {
            "hyperparams": {
                "type": "common_primitives.loss.Hyperparams",
                "kind": "RUNTIME"
            },
            "gradient_outputs": {
                "type": "d3m.primitive_interfaces.base.Gradients[d3m.container.numpy.ndarray]",
                "kind": "RUNTIME"
            },
            "fine_tune": {
                "type": "bool",
                "kind": "RUNTIME",
                "default": false
            },
            "fine_tune_learning_rate": {
                "type": "float",
                "kind": "RUNTIME",
                "default": 1e-05
            },
            "fine_tune_weight_decay": {
                "type": "float",
                "kind": "RUNTIME",
                "default": 1e-05
            },
            "timeout": {
                "type": "typing.Union[NoneType, float]",
                "kind": "RUNTIME",
                "default": null
            },
            "iterations": {
                "type": "typing.Union[NoneType, int]",
                "kind": "RUNTIME",
                "default": null
            },
            "produce_methods": {
                "type": "typing.Sequence[str]",
                "kind": "RUNTIME"
            },
            "inputs": {
                "type": "d3m.container.numpy.ndarray",
                "kind": "PIPELINE"
            },
            "outputs": {
                "type": "d3m.container.numpy.ndarray",
                "kind": "PIPELINE"
            },
            "temperature": {
                "type": "float",
                "kind": "RUNTIME",
                "default": 0
            },
            "params": {
                "type": "NoneType",
                "kind": "RUNTIME"
            }
        },
        "class_methods": {
            "can_accept": {
                "arguments": {
                    "method_name": {
                        "type": "str"
                    },
                    "arguments": {
                        "type": "typing.Dict[str, typing.Union[d3m.metadata.base.Metadata, type]]"
                    },
                    "hyperparams": {
                        "type": "common_primitives.loss.Hyperparams"
                    }
                },
                "returns": "typing.Union[NoneType, d3m.metadata.base.DataMetadata]",
                "description": "Returns a metadata object describing the output of a call of ``method_name`` method under\n``hyperparams`` with primitive arguments ``arguments``, if such arguments can be accepted by the method.\nOtherwise it returns ``None`` or raises an exception.\n\nDefault implementation checks structural types of ``arguments`` expected arguments' types\nand ignores ``hyperparams``.\n\nBy (re)implementing this method, a primitive can fine-tune which arguments it accepts\nfor its methods which goes beyond just structural type checking. For example, a primitive might\noperate only on images, so it can accept numpy arrays, but only those with semantic type\ncorresponding to an image. Or it might check dimensions of an array to assure it operates\non square matrix.\n\nPrimitive arguments are a superset of method arguments. This method receives primitive arguments and\nnot just method arguments so that it is possible to implement it without a state between calls\nto ``can_accept`` for multiple methods. For example, a call to ``fit`` could during normal execution\ninfluences what a later ``produce`` call outputs. But during ``can_accept`` call we can directly have\naccess to arguments which would have been given to ``fit`` to produce metadata of the ``produce`` call.\n\nNot all primitive arguments have to be provided, only those used by ``fit``, ``set_training_data``,\nand produce methods, and those used by the ``method_name`` method itself.\n\nParameters\n----------\nmethod_name : str\n    Name of the method which would be called.\narguments : Dict[str, Union[Metadata, type]]\n    A mapping between argument names and their metadata objects (for pipeline arguments) or types (for other).\nhyperparams : Hyperparams\n    Hyper-parameters under which the method would be called during regular primitive execution.\n\nReturns\n-------\nDataMetadata\n    Metadata object of the method call result, or ``None`` if arguments are not accepted\n    by the method."
            }
        },
        "instance_methods": {
            "__init__": {
                "kind": "OTHER",
                "arguments": [
                    "hyperparams"
                ],
                "returns": "NoneType"
            },
            "backward": {
                "kind": "OTHER",
                "arguments": [
                    "gradient_outputs",
                    "fine_tune",
                    "fine_tune_learning_rate",
                    "fine_tune_weight_decay"
                ],
                "returns": "typing.Tuple[d3m.container.numpy.ndarray, NoneType]",
                "description": "Returns the gradient with respect to inputs and with respect to params of a loss\nthat is being backpropagated end-to-end in a pipeline.\n\nThis is the standard backpropagation algorithm: backpropagation needs to be preceded by a\nforward propagation (``forward`` method).\n\nParameters\n----------\ngradient_outputs : Gradients[Outputs]\n    The gradient of the loss with respect to this primitive's output. During backpropagation,\n    this comes from the next primitive in the pipeline, i.e., the primitive whose input\n    is the output of this primitive during the forward execution with ``forward`` (and ``produce``).\nfine_tune : bool\n    If ``True``, executes a fine-tuning gradient descent step as a part of this call.\n    This provides the most straightforward way of end-to-end training/fine-tuning.\nfine_tune_learning_rate : float\n    Learning rate for end-to-end training/fine-tuning gradient descent steps.\nfine_tune_weight_decay : float\n    L2 regularization (weight decay) coefficient for end-to-end training/fine-tuning gradient\n    descent steps.\n\nReturns\n-------\nTuple[Gradients[Inputs], Gradients[Params]]\n    A tuple of the gradient with respect to inputs and with respect to params."
            },
            "fit": {
                "kind": "OTHER",
                "arguments": [
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[NoneType]",
                "description": "A noop.\n\nParameters\n----------\ntimeout : float\n    A maximum time this primitive should be fitting during this method call, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nCallResult[None]\n    A ``CallResult`` with ``None`` value."
            },
            "fit_multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling ``fit`` and after that multiple produce methods at once.\n\nParameters\n----------\nproduce_methods : Sequence[str]\n    A list of names of produce methods to call.\ninputs : Inputs\n    The inputs given to all produce methods.\ntimeout : float\n    A maximum time this primitive should take to both fit the primitive and produce outputs\n    for all produce methods listed in ``produce_methods`` argument, in seconds.\niterations : int\n    How many of internal iterations should the primitive do for both fitting and producing\n    outputs of all produce methods.\n\nReturns\n-------\nMultiCallResult\n    A dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "forward": {
                "kind": "OTHER",
                "arguments": [
                    "inputs"
                ],
                "returns": "d3m.container.numpy.ndarray",
                "description": "Similar to ``produce`` method but it is meant to be used for a forward pass during\nbackpropagation-based end-to-end training. Primitive can implement it differently\nthan ``produce``, e.g., forward pass during training can enable dropout layers, or\n``produce`` might not compute gradients while ``forward`` does.\n\nBy default it calls ``produce`` for one iteration.\n\nParameters\n----------\ninputs : Inputs\n    The inputs of shape [num_inputs, ...].\n\nReturns\n-------\nOutputs\n    The outputs of shape [num_inputs, ...]."
            },
            "get_params": {
                "kind": "OTHER",
                "arguments": [],
                "returns": "NoneType",
                "description": "A noop.\n\nReturns\n-------\nParams\n    An instance of parameters."
            },
            "gradient_output": {
                "kind": "OTHER",
                "arguments": [
                    "outputs",
                    "inputs"
                ],
                "returns": "d3m.primitive_interfaces.base.Gradients[d3m.container.numpy.ndarray]",
                "description": "Returns the gradient of loss sum_i(L(output_i, produce_one(input_i))) with respect to outputs.\n\nWhen fit term temperature is set to non-zero, it should return the gradient with respect to outputs of:\n\nsum_i(L(output_i, produce_one(input_i))) + temperature * sum_i(L(training_output_i, produce_one(training_input_i)))\n\nWhen used in combination with the ``ProbabilisticCompositionalityMixin``, it returns gradient\nof sum_i(log(p(output_i | input_i, params))) with respect to outputs.\n\nWhen fit term temperature is set to non-zero, it should return the gradient with respect to outputs of:\n\nsum_i(log(p(output_i | input_i, params))) + temperature * sum_i(log(p(training_output_i | training_input_i, params)))\n\nParameters\n----------\noutputs : Outputs\n    The outputs.\ninputs : Inputs\n    The inputs.\n\nReturns\n-------\nGradients[Outputs]\n    A structure similar to ``Container`` but the values are of type ``Optional[float]``."
            },
            "gradient_params": {
                "kind": "OTHER",
                "arguments": [
                    "outputs",
                    "inputs"
                ],
                "returns": "d3m.primitive_interfaces.base.Gradients[NoneType]",
                "description": "Returns the gradient of loss sum_i(L(output_i, produce_one(input_i))) with respect to params.\n\nWhen fit term temperature is set to non-zero, it should return the gradient with respect to params of:\n\nsum_i(L(output_i, produce_one(input_i))) + temperature * sum_i(L(training_output_i, produce_one(training_input_i)))\n\nWhen used in combination with the ``ProbabilisticCompositionalityMixin``, it returns gradient of\nlog(p(output | input, params)) with respect to params.\n\nWhen fit term temperature is set to non-zero, it should return the gradient with respect to params of:\n\nsum_i(log(p(output_i | input_i, params))) + temperature * sum_i(log(p(training_output_i | training_input_i, params)))\n\nParameters\n----------\noutputs : Outputs\n    The outputs.\ninputs : Inputs\n    The inputs.\n\nReturns\n-------\nGradients[Params]\n    A version of ``Params`` with all differentiable fields from ``Params`` and values set to gradient for each parameter."
            },
            "multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling multiple produce methods at once.\n\nWhen a primitive has multiple produce methods it is common that they might compute the\nsame internal results for same inputs but return different representations of those results.\nIf caller is interested in multiple of those representations, calling multiple produce\nmethods might lead to recomputing same internal results multiple times. To address this,\nthis method allows primitive author to implement an optimized version which computes\ninternal results only once for multiple calls of produce methods, but return those different\nrepresentations.\n\nIf any additional method arguments are added to primitive's produce method(s), they have\nto be added to this method as well. This method should accept an union of all arguments\naccepted by primitive's produce method(s) and then use them accordingly when computing\nresults.\n\nThe default implementation of this method just calls all produce methods listed in\n``produce_methods`` in order and is potentially inefficient.\n\nParameters\n----------\nproduce_methods : Sequence[str]\n    A list of names of produce methods to call.\ninputs : Inputs\n    The inputs given to all produce methods.\ntimeout : float\n    A maximum time this primitive should take to produce outputs for all produce methods\n    listed in ``produce_methods`` argument, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nMultiCallResult\n    A dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "produce": {
                "kind": "PRODUCE",
                "arguments": [
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.numpy.ndarray]",
                "singleton": false,
                "inputs_across_samples": [],
                "description": "Produce primitive's best choice of the output for each of the inputs.\n\nThe output value should be wrapped inside ``CallResult`` object before returning.\n\nIn many cases producing an output is a quick operation in comparison with ``fit``, but not\nall cases are like that. For example, a primitive can start a potentially long optimization\nprocess to compute outputs. ``timeout`` and ``iterations`` can serve as a way for a caller\nto guide the length of this process.\n\nIdeally, a primitive should adapt its call to try to produce the best outputs possible\ninside the time allocated. If this is not possible and the primitive reaches the timeout\nbefore producing outputs, it should raise a ``TimeoutError`` exception to signal that the\ncall was unsuccessful in the given time. The state of the primitive after the exception\nshould be as the method call has never happened and primitive should continue to operate\nnormally. The purpose of ``timeout`` is to give opportunity to a primitive to cleanly\nmanage its state instead of interrupting execution from outside. Maintaining stable internal\nstate should have precedence over respecting the ``timeout`` (caller can terminate the\nmisbehaving primitive from outside anyway). If a longer ``timeout`` would produce\ndifferent outputs, then ``CallResult``'s ``has_finished`` should be set to ``False``.\n\nSome primitives have internal iterations (for example, optimization iterations).\nFor those, caller can provide how many of primitive's internal iterations\nshould a primitive do before returning outputs. Primitives should make iterations as\nsmall as reasonable. If ``iterations`` is ``None``, then there is no limit on\nhow many iterations the primitive should do and primitive should choose the best amount\nof iterations on its own (potentially controlled through hyper-parameters).\nIf ``iterations`` is a number, a primitive has to do those number of iterations,\nif possible. ``timeout`` should still be respected and potentially less iterations\ncan be done because of that. Primitives with internal iterations should make\n``CallResult`` contain correct values.\n\nFor primitives which do not have internal iterations, any value of ``iterations``\nmeans that they should run fully, respecting only ``timeout``.\n\nParameters\n----------\ninputs : Inputs\n    The inputs of shape [num_inputs, ...].\ntimeout : float\n    A maximum time this primitive should take to produce outputs during this method call, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nCallResult[Outputs]\n    The outputs of shape [num_inputs, ...] wrapped inside ``CallResult``."
            },
            "set_fit_term_temperature": {
                "kind": "OTHER",
                "arguments": [
                    "temperature"
                ],
                "returns": "NoneType",
                "description": "Sets the temperature used in ``gradient_output`` and ``gradient_params``.\n\nParameters\n----------\ntemperature : float\n    The temperature to use, [0, inf), typically, [0, 1]."
            },
            "set_params": {
                "kind": "OTHER",
                "arguments": [
                    "params"
                ],
                "returns": "NoneType",
                "description": "A noop.\n\nParameters\n----------\nparams : Params\n    An instance of parameters."
            },
            "set_training_data": {
                "kind": "OTHER",
                "arguments": [],
                "returns": "NoneType",
                "description": "A noop.\n\nParameters\n----------"
            }
        },
        "class_attributes": {
            "logger": "logging.Logger",
            "metadata": "d3m.metadata.base.PrimitiveMetadata"
        },
        "instance_attributes": {
            "hyperparams": "d3m.metadata.hyperparams.Hyperparams",
            "random_seed": "int",
            "docker_containers": "typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]",
            "volumes": "typing.Dict[str, str]",
            "temporary_directory": "typing.Union[NoneType, str]"
        }
    },
    "structural_type": "common_primitives.loss.Loss",
    "description": "A primitive that produces an end-to-end (pipeline) loss, for the purpose of\ninitiating backpropagation and fine-tuning (or training) a whole pipeline.\nThis primitive is only needed during the end-to-end training of a pipeline and\nis not intended to be a part of the pipeline after the pipeline is trained.\nForward pass See test_end_to_end.py in common_primitives repo for a running\nexample of the setup. This loss primitive is conceptually the same as the\nCriterion module in Torch and PyTorch.\n\nAttributes\n----------\nmetadata : PrimitiveMetadata\n    Primitive's metadata. Available as a class attribute.\nlogger : Logger\n    Primitive's logger. Available as a class attribute.\nhyperparams : Hyperparams\n    Hyperparams passed to the constructor.\nrandom_seed : int\n    Random seed passed to the constructor.\ndocker_containers : Dict[str, DockerContainer]\n    A dict mapping Docker image keys from primitive's metadata to (named) tuples containing\n    container's address under which the container is accessible by the primitive, and a\n    dict mapping exposed ports to ports on that address.\nvolumes : Dict[str, str]\n    A dict mapping volume keys from primitive's metadata to file and directory paths\n    where downloaded and extracted files are available to the primitive.\ntemporary_directory : str\n    An absolute path to a temporary directory a primitive can use to store any files\n    for the duration of the current pipeline run phase. Directory is automatically\n    cleaned up after the current pipeline run phase finishes.",
    "digest": "1facfd826227375ca34a1af63e16ffc1cd465ce40f6c4ee7b392104c094392ff"
}
